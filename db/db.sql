create table movie(
    movie_id integer,
    movie_title varchar(100),
    release_date varchar(100),
    movie_time varchar(50),
    director_name varchar(100)
);

insert into movie values(1,"dangal","21-10-2019","9:00PM","rajkumar");
insert into movie values(2,"pushpa","21-10-2021","12:00PM","Hirani");