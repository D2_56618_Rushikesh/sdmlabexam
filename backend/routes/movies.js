const express=require('express')
const db=require('../db')
const utils=require('../utils')

const router=express.Router()

router.get('/:name',(request,response)=>{

    const { name } =request.params
    const querry=
    `
    select * from movie where movie_title='${name}'
    `
    db.execute(querry,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.post('/',(request,response)=>{

    const { id, title, release, time, directorname }=request.body
    const querry=
    `
    insert into movie values('${id}','${title}','${release}','${time}','${directorname}')
    `
    db.execute(querry,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.put('/:id',(request,response)=>{
    const {id}=request.params

    const { release, time }=request.body
    const querry=
    `
    update movie set release_date='${release}', movie_time='${time}' where movie_id='${id}'
    `
    db.execute(querry,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.delete('/:id',(request,response)=>{
    const {id}=request.params

    const querry=
    `
    delete from movie where movie_id='${id}'
    `
    db.execute(querry,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

module.exports=router